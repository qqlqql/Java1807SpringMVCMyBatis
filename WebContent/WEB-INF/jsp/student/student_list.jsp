<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link rel="stylesheet" href="${ctx}/lib/bootstrap-3.3.7-dist/css/bootstrap.css" />
	</head>
	<body>
		<!--导航开始-->
   <nav class="navbar navbar-default">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#" class="active">教务管理系统</a>
	    </div>
	    
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="${ctx}/student/pagelist.action"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;学生管理<span class="sr-only"></span></a></li>
	        <li><a href="${ctx}/banji/pagelist.action"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;班级管理</a></li>
	        <li><a href="${ctx}/course/pagelist.action"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;课程管理</a></li>
	        <li><a href="${ctx}/jiaowu1/pagelist.action"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;教务管理</a></li>
	        <li><a href="online_user_list.jsp"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;在线管理员</a></li>
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="${pageContext.request.contextPath}/login1/logout.action">欢迎:${user.name}<span class="glyphicon glyphicon-off" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;退出</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>	
	<!--导航结束-->
	<!--内容部分-->
	
	<div class="container">
		<div class="row">
			<!--左边内容开始-->
			<div class="col-md-2">
				<div class="list-group">
				  <a href="${pageContext.request.contextPath}/student/pagelist.action" class="list-group-item active">
				   学生列表
				  </a>
				  <a href="javascript:insert()" class="list-group-item">学生添加</a>
				</div>
			</div>
			<!--左边内容结束-->
			<!--右边内容开始-->
			<a href="javascript:deleteAll()">批量删除</a>
			<div class="col-md-10">
			<form action="${pageContext.request.contextPath}/student/pagelist.action" id="searchFrom" method="post" >
			   <input type="hidden" id="pageNo" name="pageNo"/>
			姓名：<input type="text" name="name" value="${searchCondition.name }" />
			年龄：<input type="text" name="age" value="${searchCondition.age }" />
			性别：<select name="gender" value="${searchCondition.gender}">
			    <option value="">不限</option>
			    <option value="男">男</option>
			    <option value="女">女</option>
			</select>
			 <input type="submit" value="搜索"/> 
			
			</form>
			<br/>
			<form action="" id="mainform" method="post">
				 <table class="table table-hover">
                     <tr>
                       <th><input type="checkbox"  id="selectAlls" onclick="selectAll()" /></th>
                       <th>id</th>
                       <th>姓名</th>
                       <th>年龄</th>
                       <th>性别</th>
                       <th>班级id</th>
                       <th>班级</th>
                       
                     </tr>
               <c:forEach items="${list}" var="student">
                   	<tr>
                   	    <td><input type="checkbox" name="selectIds" value="${student.id}"/></td>
                   		<td>${student.id}</td>
                        <td>${student.name}</td>
                        <td>${student.age}</td>
                        <td>${student.banji.id}</td>
                        <td>${student.banji.name}</td>
                   	</tr>
                   	</c:forEach>
				</table>
			</form>
				</div>
				<div>
				<nav style="text-align: center;" aria-label="Page navigation">
		       	<ul class="pagination content">
				<!-- 上一页 开始 -->
				<c:choose>
				<c:when test="${pageBean.pageNo == 1}">
				<li class="disabled"><a href="#" aria-label="Previous"> 
				<spanaria-hidden="true">&laquo;</span></a>
				</li>
				</c:when>
				<c:when test="${pageBean.pageNo != 1}">
				<li><a href="<%=request.getContextPath()%>/student/pagelist.action?pageNo=${pageBean.pageNo-1}&pageSize=3"aria-label="Previous"> 
					<span aria-hidden="true">&laquo;</span>
				</a></li>
				</c:when>
				</c:choose>
				<!-- 上一页 结束  -->
				<%-- <ul class="pagination">
					<c:forEach begin="1" end="${pageBean.totalPage}"  var="page">
					<c:if test="${pageBean.pageNo == page}">
					<li class="active"><a href="#">${page}</a></li>
					</c:if>
					<c:if test="${pageBean.pageNo !== page}">
					   <li><a href="javascript:goPage(${page})">${page}</a></li>
					</c:if>
					</c:forEach>
				</ul> --%>
			    <c:forEach begin="1" end="${pageBean.totalPage }" var="i" >
		  		<c:choose>
		  			<c:when test="${pageBean.pageNo == i}">
				<li class="active"><a href="#">${i}</a></li>
					</c:when>
					<c:otherwise>
					 <li><a href="javascript:goPage(${i})">${i}</a></li>
				<%-- <li><a href="<%=request.getContextPath()%>/student?method=pagelist&pageNo=${i}&pageSize=3">${i}</a></li> --%>
					</c:otherwise>
				</c:choose>
				 </c:forEach>
				<!-- 下一页 开始 -->
				<c:choose>
				<c:when test="${pageBean.pageNo == pageBean.totalPage}">
				<li class="disabled"><a href="#" aria-label="Previous"> 
				<span aria-hidden="true" >&raquo;</span>
				</a></li>
				</c:when>
				<c:when test="${pageBean.pageNo  != pageBean.totalPage}">
				<li><a
					href="${pageContext.request.contextPath}/student/pagelist.action?pageNo=${pageBean.pageNo + 1}&pageSize=3"aria-label="Previous"> 
					<span aria-hidden="true">&raquo;</span>
				</a></li>
				</c:when>
				</c:choose>
				<!-- 下一页 结束 -->
			</ul>
			</nav> 
			</div>	
			
				
				<!--右边部分结束-->
			</div>
		</div>
	    <script src="${pageContext.request.contextPath}/lib/jquery/jquery-1.11.1.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath}/lib/layer/layer.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath}/js/mylayer.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/lib/bootstrap-3.3.7-dist/js/bootstrap.js" ></script>
	
		  <script type="text/javascript">
		  function goPage(i){
			  $("#pageNo").val(i);
			  $("#searchFrom").submit();
		  }
         function selectAll(){
	    	//得到上面全选、反选按钮的状态
	    	var isChecked= $("#selectAlls").prop("checked");
	    	//下面所有checkbox的状态和上面全选、反选checkbox的状态一致
	    	$("input[name=selectIds]").prop("checked",isChecked);
	    }
        function deleteAll(){
        	$("#mainform").attr("action","${pageContext.request.contextPath}/student/deleteAll.action");
        	$("#mainform").submit();
        } 
		function deleteById(id) {
			//用户点了确定，confirm返回的是true，
			//用户点了取消，confirm返回的是false，
			/*  mylayer.confirm("您确认要删除么", "${pageContext.request.contextPath}/student/deleteById.action?id=" + id); */
			layer.confirm(
			   "你确定要删除吗",
				function(index)	{
					console.log("点击了确定");
					$.post(
						"${pageContext.request.contextPath}/student/deleteById.action?id=" + id,
								function(data){
							    if(data){
							    	mylayer.success("删除成功");
							    	location.reload();
							    }else{
							    	mylayer.errorMsg("删除失败");
							    }
						},
						
						"json"
					);
				},
				function(index){
					console.log("点击了取消");
				}
			);
			/* var isDel = confirm("您确认要删除么？");
			if (isDel) {
				location.href ="${pageContext.request.contextPath}/student?method=deleteById&id=" + id;
			}  */
		}
		function insert(){
			layer.open({
				type:2,//iframe
				title:"添加学生",
				area:["600px","600px"],//弹出框的宽和高
				offset:"100px",
				content:"${pageContext.request.contextPath}/student/getInsertPage.action"
			});
		}
		
		
	</script>
		
	</body>
</html>
