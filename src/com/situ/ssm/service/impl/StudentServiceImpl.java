package com.situ.ssm.service.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.situ.ssm.dao.IStudentDao;
import com.situ.ssm.entity.Student;
import com.situ.ssm.service.IStudentService;
import com.situ.ssm.util.MyBatisUtil;
import com.situ.ssm.vo.PageBean;
import com.situ.ssm.vo.StudentSearchCondition;

public class StudentServiceImpl implements IStudentService {
	private IStudentDao StudentDao = null;

	public StudentServiceImpl() {
		SqlSession sqlSession = MyBatisUtil.getSqlSession();
		StudentDao = sqlSession.getMapper(IStudentDao.class);
	}

	@Override
	public List<Student> list() {

		return StudentDao.list();
	}

	@Override
	public PageBean<Student> getPageBean(StudentSearchCondition condition) {
		PageBean<Student> pageBean = new PageBean<Student>();
		pageBean.setPageNo(condition.getPageNo());
		pageBean.setPageSize(condition.getPageSize());
		// 总记录数（是符合condition条件的总记录数）
		int totalCount = StudentDao.getTotalCount(condition);
		pageBean.setTotalCount(totalCount);
		// 一共有多少页
		int totalPage = (int) Math.ceil((double) totalCount / condition.getPageSize());
		pageBean.setTotalPage(totalPage);
		// 得到当前页的数据
		int offset = (condition.getPageNo() - 1) * condition.getPageSize();
		List<Student> list = StudentDao.findPageBeanListByCondition(condition, offset);
		pageBean.setList(list);

		return pageBean;
		
	}

	@Override
	public boolean insert(Student student) {
		int count=StudentDao.insert(student);
		return count==1 ? true : false;
	}

	@Override
	public List<Student> one2oneList() {
		return StudentDao.one2oneList();
	}
	}



	
