package com.situ.ssm.service.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.situ.ssm.dao.IBanjiDao;
import com.situ.ssm.dao.IStudentDao;
import com.situ.ssm.entity.Banji;
import com.situ.ssm.entity.Student;
import com.situ.ssm.service.IBanjiService;
import com.situ.ssm.util.MyBatisUtil;

public class BanjiServiceImpl implements IBanjiService {
   private   IBanjiDao banjiDao=null;
   private SqlSession sqlSession = null;
   
   public BanjiServiceImpl() {
	    sqlSession = MyBatisUtil.getSqlSession();
	    banjiDao = sqlSession.getMapper(IBanjiDao.class);
}
	@Override
	public Banji findBanjiInfoById(Integer id) {
		return banjiDao.findBanjiInfoById(id);
	}
	@Override
	public List<Banji> findBanjiCourseInfo() {
       	return banjiDao.findBanjiCourseInfo();
	}
	/*@Override
	public List<Student> findStudentInfo() {
		return banjiDao.findStudentInfo();
	}*/

}
