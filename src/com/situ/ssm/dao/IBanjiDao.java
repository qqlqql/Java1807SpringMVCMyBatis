package com.situ.ssm.dao;

import java.util.List;

import com.situ.ssm.entity.Banji;
import com.situ.ssm.entity.Student;

public interface IBanjiDao {

	Banji findBanjiInfoById(Integer id);

	List<Banji> findBanjiCourseInfo();

	/*List<Student> findStudentInfo();*/

}
