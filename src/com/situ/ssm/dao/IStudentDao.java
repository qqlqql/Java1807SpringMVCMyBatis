package com.situ.ssm.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.situ.ssm.entity.Student;
import com.situ.ssm.vo.PageBean;
import com.situ.ssm.vo.StudentSearchCondition;

public interface IStudentDao {
	
	List<Student>list();

	PageBean<Student> getPageBean();

	int getTotalCount(StudentSearchCondition condition);

	List<Student> findPageBeanListByCondition(@Param("condition")StudentSearchCondition condition, @Param("offset")Integer offset);

	int insert(Student student);

	List<Student> one2oneList();
}
