package com.situ.ssm.entity;

import java.util.ArrayList;
import java.util.List;

public class Course {
	private Integer id;
	private String name;
	private Integer credit;
    private List<Banji>list=new ArrayList<>();
	public Course(Integer id, String name, Integer credit, List<Banji> list) {
		super();
		this.id = id;
		this.name = name;
		this.credit = credit;
		this.list = list;
	}

	public List<Banji> getList() {
		return list;
	}

	public void setList(List<Banji> list) {
		this.list = list;
	}

	public Course(String name, Integer credit) {
		super();
		this.name = name;
		this.credit = credit;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCredit() {
		return credit;
	}

	public void setCredit(Integer credit) {
		this.credit = credit;
	}

	public Course(Integer id, String name, Integer credit) {
		super();
		this.id = id;
		this.name = name;
		this.credit = credit;
	}

	public Course() {
		
	}

	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", credit=" + credit + ", list=" + list + "]";
	}
}
