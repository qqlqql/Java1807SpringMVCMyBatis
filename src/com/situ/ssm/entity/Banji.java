package com.situ.ssm.entity;

import java.util.ArrayList;
import java.util.List;

public class Banji {
	private Integer id;
	private String name;

	// 一个班级可以选很多课
	private List<Course> courseList = new ArrayList<>();

	// 一个班级可以有很多学生
	private List<Student> list = new ArrayList<>();

	public Banji(Integer id, String name, List<Course> courseList, List<Student> list) {
		super();
		this.id = id;
		this.name = name;
		this.courseList = courseList;
		this.list = list;
	}

	public List<Course> getCourseList() {
		return courseList;
	}

	public void setCourseList(List<Course> courseList) {
		this.courseList = courseList;
	}

	public List<Student> getList() {
		return list;
	}

	public void setList(List<Student> list) {
		this.list = list;
	}

	public Banji() {
		super();
	}

	public Banji(Integer id, String name, List<Student> list) {
		super();
		this.id = id;
		this.name = name;
		this.list = list;
	}

	public Banji(String name) {
		super();
		this.name = name;
	}

	public Banji(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Banji [id=" + id + ", name=" + name + ", list=" + list + ", courseList=" + courseList + "]";
	}

}
