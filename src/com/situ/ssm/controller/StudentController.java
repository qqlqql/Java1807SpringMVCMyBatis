package com.situ.ssm.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.situ.ssm.entity.Student;
import com.situ.ssm.service.IStudentService;
import com.situ.ssm.service.impl.StudentServiceImpl;
import com.situ.ssm.vo.PageBean;
import com.situ.ssm.vo.StudentSearchCondition;

@Controller
@RequestMapping(value = "/student")
public class StudentController {
	private IStudentService studentService = new StudentServiceImpl();

	@RequestMapping(value = "/list")
	@ResponseBody
	public List<Student> list() {
		List<Student> list = studentService.list();
		return list;
	}

	// 分页
	@RequestMapping(value = "/pagelist")
	@ResponseBody
	public PageBean<Student> pagelist(StudentSearchCondition condition) {
		if (condition.getPageNo() == null) {
			condition.setPageNo(1);
		}
		PageBean<Student> pageBean = studentService.getPageBean(condition);

		return pageBean;
	}

	// 添加
	@RequestMapping(value = "/insert")
	@ResponseBody
	public boolean insert(Student student) {
		boolean result = studentService.insert(student);
		return result;
	}

	@RequestMapping(value="/one2oneListJson")
	@ResponseBody
	public List<Student> one2oneListJson() {
		return studentService.one2oneList();
	}
	
	@RequestMapping(value="/one2oneList")
	public String one2oneList(Model model) {
		List<Student> list = studentService.one2oneList();
		model.addAttribute("list", list);
		return "/student/one2oneList";
	}
	
}
