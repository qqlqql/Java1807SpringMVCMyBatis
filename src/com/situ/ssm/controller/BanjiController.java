package com.situ.ssm.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.situ.ssm.entity.Banji;
import com.situ.ssm.entity.Student;
import com.situ.ssm.service.IBanjiService;
import com.situ.ssm.service.impl.BanjiServiceImpl;
import com.situ.ssm.service.impl.StudentServiceImpl;

@Controller
@RequestMapping(value = "/banji")
public class BanjiController {

	private IBanjiService banjiService = new BanjiServiceImpl();

	@RequestMapping(value = "/findBanjiInfoById")
	@ResponseBody
	public Banji findBanjiInfoById(Integer id) {
		return banjiService.findBanjiInfoById(id);
	}

	@RequestMapping(value = "/findBanjiCourseInfo")
	@ResponseBody
	public List<Banji> findBanjiCourseInfo() {
		return banjiService.findBanjiCourseInfo();
	}
	/*@RequestMapping(value="/findStudentInfo")
	@ResponseBody
    public List<Student> findStudentInfo(){
    	return banjiService.findStudentInfo();
    }*/
}
